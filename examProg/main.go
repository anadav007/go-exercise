package main

import (
	"examProg/config"
	"examProg/controllers"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	_ "github.com/mattn/go-sqlite3"
)

func main() {
	db := config.GetDB() //Establishing connection to DBase
	router := mux.NewRouter()

	var hostController controllers.HostController
	var containerController controllers.ContainerController

	//API

	//Host
	router.HandleFunc("/host", controllers.HostController.GetAllHosts(hostController, db)).Methods("GET")
	router.HandleFunc("/host/{id}", controllers.HostController.GetHostById(hostController, db)).Methods("GET")
	//Container
	router.HandleFunc("/container", controllers.ContainerController.GetAllContainers(containerController, db)).Methods("GET")
	router.HandleFunc("/container/{id}", controllers.ContainerController.GetContainerById(containerController, db)).Methods("GET")
	router.HandleFunc("/container/hosted/{host_id}", controllers.ContainerController.GetContainersByHostId(containerController, db)).Methods("GET")
	router.HandleFunc("/container/", controllers.ContainerController.AddContainer(containerController, db)).Methods("POST")

	//Informs at which port the server is running:
	fmt.Println("Server is running at port: 8080")
	log.Fatal(http.ListenAndServe(":8080", router))

	http.ListenAndServe(":"+"8080", router)
}
