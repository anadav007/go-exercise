package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

//The function opens each file according to the name it receives and returns it as a string
func LoadFile(fileName string) (string, error) {
	bytes, err := ioutil.ReadFile((fileName))
	if err != nil {
		return "", err
	}
	return string(bytes), nil
}

//The function runs the home page of the site with an HTML file
func LobbyPage(w http.ResponseWriter, r *http.Request) {
	var html, err = LoadFile("lobby.html")

	if err != nil {
		w.WriteHeader(404)
	}

	fmt.Fprint(w, html)
}

//checks if string is in a json format
func IsJson(str string) bool {
	var js json.RawMessage
	return json.Unmarshal([]byte(str), &js) == nil
}

//Causes JSON to be arranged for on-screen display. Returns a string
//*Simple JSON only (does not work with JSON within JSON)*\\
func ArrangingJsonInformation(json string) string {
	str := ""

	Stringhosts := json

	if IsJson(json) {
		//Checks if the JSON starts with "[" or with "{". Then delete them (of the edge)
		if json[0] == '[' {
			Stringhosts = strings.Replace(Stringhosts, "[{", "", -1)
			Stringhosts = strings.Replace(Stringhosts, "}]", "", -1)
		} else {
			Stringhosts = Stringhosts[1 : len(Stringhosts)-2]
		}

		Stringhosts = strings.Replace(Stringhosts, "\"", "", -1) //Deletes all '"'

		Stringhosts = strings.Replace(Stringhosts, "},", "}", -1) //Deletes all "," between two parts of the JSON (not of values ​​within the parts)

		listhosts := strings.Split(Stringhosts, "}{") //Divides JSON by its parts

		//For each part in JSON
		for _, listhost := range listhosts {
			listhost := strings.Split(listhost, ",") //Disassemble each part into its own values

			//For each entry it adds the value within a neat JSON string
			for _, parameter := range listhost {
				str += parameter + "\t"
			}
			str += "\n"
		}
		return str
	}
	return json
}

//The function receives the path entered by the user and according to the path prints to the user the information from the API
func LoadPage(w http.ResponseWriter, r *http.Request, path string) {

	resp, err := http.Get("http://localhost:8080" + path) //Gets the information from the API by the URL

	if err != nil {
		fmt.Fprintf(w, err.Error())
		return
	}

	body, err := ioutil.ReadAll(resp.Body) //// read response body
	if err != nil {
		fmt.Fprintf(w, err.Error())
		return
	}

	fmt.Fprintf(w, ArrangingJsonInformation(string(body)))

}

//webServer
func WebServer(w http.ResponseWriter, r *http.Request) {

	//Lobby page of website
	if r.URL.Path == "/" {
		LobbyPage(w, r)
		return
	}

	//Two user access points on the site (host and container)
	if r.URL.Path[0:5] == "/host" || r.URL.Path[0:10] == "/container" {

		LoadPage(w, r, r.URL.Path)
		return
	}

	http.Error(w, "404 not found.", http.StatusNotFound)
}

func main() {
	http.HandleFunc("/", WebServer)
	fmt.Println("http://localhost:8081")
	http.ListenAndServe(":8081", nil) //The server starts listening in port 8081
}
